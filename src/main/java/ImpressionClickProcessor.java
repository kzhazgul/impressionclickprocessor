import org.apache.kafka.clients.producer.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.functions;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class ImpressionClickProcessor {
    private static final String RAW_DATA_DIR = "raw_data";
    private static final String KAFKA_TOPIC = "impressions_clicks";
    private static final String PROCESSED_FILES_TRACKER = "processed_files.txt";

    public static void main(String[] args) {
        String userAgent = args.length > 0 ? args[0] : "some user agent";

        SparkSession spark = SparkSession.builder()
                .appName("ImpressionClickProcessor")
                .master("local[*]")
                .getOrCreate();

        Dataset<Row> rawData = readRawData(spark, userAgent);
        Dataset<Row> result = processRawData(rawData);

        sendToKafka(result, userAgent);
    }

    private static Dataset<Row> readRawData(SparkSession spark, String userAgent) {
        List<String> processedFiles = getProcessedFiles();

        File folder = new File(RAW_DATA_DIR);
        File[] files = folder.listFiles((dir, name) -> name.endsWith(".parquet") && !processedFiles.contains(name));

        Dataset<Row> dataset = null;
        if (files != null) {
            for (File file : files) {
                Dataset<Row> data = spark.read().parquet(file.getAbsolutePath());
                data = data.filter(functions.col("device_settings.user_agent").equalTo(userAgent));
                dataset = data;
                markFileAsProcessed(file.getName());
            }
        }

        return dataset;
    }

    private static Dataset<Row> processRawData(Dataset<Row> data) {
        return data.withColumn("datetime", functions.from_unixtime(functions.col("timestamp").divide(1000)))
                .withColumn("date_hour", functions.date_format(functions.col("datetime"), "yyyy-MM-dd HH"))
                .groupBy("date_hour", "device_settings.user_agent")
                .agg(functions.count("*").alias("count"))
                .select("date_hour", "user_agent", "count");
    }

    private static void sendToKafka(Dataset<Row> data, String userAgent) {
        String kafkaBootstrapServers = "localhost:9092";

        Properties props = new Properties();
        props.put("bootstrap.servers", kafkaBootstrapServers);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

        data.toJSON().collectAsList().forEach(json -> {
            ProducerRecord<String, String> record = new ProducerRecord<>(KAFKA_TOPIC, userAgent, json);
            producer.send(record);
        });

        producer.close();
    }

    private static List<String> getProcessedFiles() {
        try {
            Path path = Paths.get(PROCESSED_FILES_TRACKER);
            if (Files.exists(path)) {
                return Files.readAllLines(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private static void markFileAsProcessed(String fileName) {
        try {
            Files.write(Paths.get(PROCESSED_FILES_TRACKER), (fileName + "\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
